---
title: 'Alpine 3.13.12, 3.14.8, 3.15.6 and 3.16.2 released'
date: 2022-08-09
---

Alpine 3.13.12, 3.14.8 and 3.15.6 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.13.12](https://git.alpinelinux.org/aports/log/?h=v3.13.12)
- [3.14.8](https://git.alpinelinux.org/aports/log/?h=v3.14.8)
- [3.15.6](https://git.alpinelinux.org/aports/log/?h=v3.15.6)
- [3.16.2](https://git.alpinelinux.org/aports/log/?h=v3.16.2)

Those releases fixes zlib
[CVE-2022-37434](https://security.alpinelinux.org/vuln/CVE-2022-37434).

