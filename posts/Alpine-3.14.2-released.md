---
title: 'Alpine 3.14.2 released'
date: 2021-08-27
---

Alpine Linux 3.14.2 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.14.2 of its Alpine Linux operating system.

This release includes fixes for openssl [CVE-2021-3711](https://security.alpinelinux.org/vuln/CVE-2021-3711)
and [CVE-2021-3712](https://security.alpinelinux.org/vuln/CVE-2021-3712).

The full lists of changes can be found in the [git
log](https://git.alpinelinux.org/aports/log/?h=v3.14.2).

Git Shortlog
------------

<pre>
6543 (1):
      community/gitea: upgrade to 1.14.6

Andy Postnikov (10):
      main/postgresql: security upgrade to 13.4 (CVE-2021-3677)
      community/composer: upgrade to 2.1.6
      community/icinga2: security upgrade to 2.12.6 (CVE-2021-37698)
      main/pgpool: upgrade to 4.2.4
      community/stunnel: upgrade to 5.60
      community/php8: improve, build php only twice: apache2+cgi and the rest
      community/php8: security upgrade to 8.0.10
      main/nodejs: security upgrade to 14.17.5
      community/php7: security upgrade to 7.4.23
      community/php7: improve build

Anjandev Momi (1):
      community/prosody: upgrade to 0.11.10

Ariadne Conill (4):
      main/gpsd: move back from community
      main/ruby: add CVE-2021-31799 to secfixes
      main/asterisk: add mitigation for CVE-2021-32558
      main/libspf2: add mitigation for CVE-2021-20314

Bart Ribbers (8):
      community/plasma: upgrade to 5.22.4
      community/plasma-workspace: rebuild against gpsd 3.23
      community/marble: rebuild against gpsd 3.23
      community/stellarium: rebuild against gpsd 3.23 and modernize
      community/kasts: add missing dep on qt5-qtbase-sqlite
      community/amazfish: upgrade to 2.0.2
      community/sddm: improve init script
      community/plasma-phone-components: backport fix for screenshots

Daniel Néri (1):
      main/unbound: upgrade to 1.13.2

Duncan Bellamy (1):
      community/dovecot-fts-xapian: upgrade to 1.4.12

J0WI (11):
      community/apache-ant: security upgrade to 1.10.11
      main/ruby: security upgrade to 2.7.4
      community/firefox-esr: security upgrade to 78.12.0
      community/openjdk7: upgrade to 2.6.25
      community/openjdk7: security upgrade to 7.301.2.6.26
      main/mariadb: security upgrade to 10.5.12
      community/rust: upgrade to 1.52.1
      community/rust: patch CVE-2021-29922
      community/mozjs78: security upgrade to 78.13.0
      community/tor: security upgrade to 0.4.5.10
      main/openssl: security upgrade to 1.1.1l

Kevin Daudt (18):
      community/uglifycss: remove node_modules/root folder
      community/uglify-js: remove node_modules/root folder
      main/c-ares: security upgrade to 1.17.2 (CVE-2021-3672)
      community/bupstash: rebuild to mitigate CVE-2021-29922
      community/corrosion: rebuild to mitigate CVE-2021-29922
      community/git-metafile: rebuild to mitigate CVE-2021-29922
      community/librsvg: rebuild to mitigate CVE-2021-29922
      community/mozjs78: rebuild to mitigate CVE-2021-29922
      community/newsflash: rebuild to mitigate CVE-2021-29922
      community/squeekboard: rebuild to mitigate CVE-2021-29922
      community/suricata: rebuild to mitigate CVE-2021-29922
      community/tokei: rebuild to mitigate CVE-2021-29922
      community/alacritty: rebuild to mitigate CVE-2021-29922
      community/salt: upgrade to 3003.2
      main/bind: security upgrade to 9.16.20 (CVE-2021-25218)
      main/bind: patch map format
      community/chezmoi: upgrade to 2.0.16
      community/libssh: security upgrade to 0.9.6 (CVE-2021-3634)

Leo (4):
      community/fetchmail: security upgrade to 6.4.20
      community/gpsd: upgrade to 3.23
      main/grep: upgrade to 3.7
      community/wireshark: upgrade to 3.4.8

Leonardo Arena (3):
      community/nextcloud: upgrade to 21.0.4
      community/nextcloud20: upgrade to 20.0.12
      community/zabbix: upgrade to 5.4.3 and fix proc.num support in Agent 2

Michał Polański (2):
      community/hcloud: upgrade to 1.26.1
      community/fuse-overlayfs: upgrade to 1.7.1

Milan P. Stanić (2):
      main/haproxy: security upgrade to 2.4.3
      main/haproxy: don't override CFLAGS in make

Natanael Copa (33):
      community/go: upgrade to 1.16.7 (CVE-2021-36221)
      main/linux-lts: upgrade to 5.10.59
      community/jool-modules-lts: rebuild against kernel 5.10.59-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.59-r0
      community/rtpengine-lts: rebuild against kernel 5.10.59-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.59-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.59-r0
      main/zfs-lts: rebuild against kernel 5.10.59-r0
      main/busybox-initscripts: create /dev/virtio-ports symlinks
      main/busybox-initscripts: fix mdev.conf to work with mdev -s
      main/linux-rpi: upgrade to 5.10.60
      community/jool-modules-rpi: rebuild against kernel 5.10.60-r0
      main/zfs-rpi: rebuild against kernel 5.10.60-r0
      main/linux-lts: upgrade to 5.10.60
      community/jool-modules-lts: rebuild against kernel 5.10.60-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.60-r0
      community/rtpengine-lts: rebuild against kernel 5.10.60-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.60-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.60-r0
      main/zfs-lts: rebuild against kernel 5.10.60-r0
      main/linux-rpi: upgrade to 5.10.61
      community/jool-modules-rpi: rebuild against kernel 5.10.61-r0
      main/zfs-rpi: rebuild against kernel 5.10.61-r0
      main/linux-lts: upgrade to 5.10.61
      community/jool-modules-lts: rebuild against kernel 5.10.61-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.61-r0
      community/rtpengine-lts: rebuild against kernel 5.10.61-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.61-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.61-r0
      main/zfs-lts: rebuild against kernel 5.10.61-r0
      scripts/mkimg.base.sh: include gnu wget
      main/raspberrypi-bootloader: upgrade to 1.20210805
      ===== release 3.14.2 =====

Oliver Smith (1):
      community/pmbootstrap: upgrade to 1.36.0

Thomas Liske (3):
      community/py3-pyroute2: fix empty ipset content
      community/frr: fix reload script and add checkpath to initd
      community/ifstate: upgrade to 1.5.5

wener (1):
      community/tinc-pre: upgrade to 1.1pre18


</pre>
