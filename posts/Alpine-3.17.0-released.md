---
title: 'Alpine 3.17.0 released'
date: 2022-11-22
---

Alpine Linux 3.17.0 Released
============================

We are pleased to announce the release of Alpine Linux 3.17.0, the first in
the v3.17 stable series.

<a name="highlights">Highlights</a>
----------

* bash [5.2](https://lists.gnu.org/archive/html/bash-announce/2022-09/msg00000.html)
* GCC [12](https://gcc.gnu.org/gcc-12/changes.html)
* Kea [2.2](https://www.isc.org/blogs/kea-2-2-0/)
* LLVM [15](https://releases.llvm.org/15.0.0/docs/ReleaseNotes.html)
* OpenSSL [3.0](https://www.openssl.org/blog/blog/2021/09/07/OpenSSL3.Final/)
* Perl [5.36](https://perldoc.perl.org/perldelta)
* PostgreSQL [15](https://www.postgresql.org/about/news/postgresql-15-released-2526/)
* Node.js (lts) [18.12](https://nodejs.org/en/blog/release/v18.12.0/)
* Node.js (current) [19.1](https://nodejs.org/en/blog/release/v19.1.0/)
* Ceph [17.2](https://ceph.io/en/news/blog/2022/v17-2-0-quincy-released/)
* GNOME [43](https://release.gnome.org/43/)
* Go [1.19](https://go.dev/blog/go1.19)
* KDE Plasma [5.26](https://kde.org/announcements/plasma/5/5.26.0/)
* Rust [1.64](https://blog.rust-lang.org/2022/09/22/Rust-1.64.0.html)
* .NET [7.0.100](https://github.com/dotnet/core/blob/main/release-notes/7.0/7.0.0/7.0.0.md)

<a name="significant_changes">Significant changes</a>
-------------------
OpenSSL 3.0 is now the default OpenSSL version. OpenSSL 1.1 is available
via the openssl1.1-compat package.

Rust is now available on all supported architectures.

<a name="upgrade_notes">Upgrade notes</a>
-------------

As always, make sure to use `apk upgrade --available` when switching between
  major versions.

<a name="deprecation_notes">Deprecation notes</a>
-----------------
PHP 8.0 has been deprecated.

ISC Kea moved to main repository for long time support while ISC dhcp
moved to community repository. Users of dhcpd are encouraged to migrate 
to Kea.

<a name="changes">Changes</a>
-------

The full list of changes can be found in the [wiki][7],  [git log][8] and [bug tracker][9].

<a name="credits">Credits</a>
-------

Thanks to everyone sending patches, bug reports, new and updated aports,
and to everyone helping with writing documentation, maintaining the
infrastructure, or contributing in any other way!

Thanks to [GIGABYTE][1], [Linode][2], [Fastly][3], [IBM][4], [Equinix Metal][5],
[vpsFree][6] for providing us with hardware and hosting.

[1]: https://www.gigabyte.com/
[2]: https://linode.com
[3]: https://www.fastly.com/
[4]: https://ibm.com/
[5]: https://www.equinix.com/
[6]: https://vpsfree.org
[7]: https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.17.0
[8]: https://git.alpinelinux.org/cgit/aports/log/?h=v3.17.0
[9]: https://gitlab.alpinelinux.org/alpine/aports/issues?scope=all&utf8=%E2%9C%93&state=closed&milestone_title=3.17.0

### aports Commit Contributors

<pre>
2cgc5h
3np
6543
Adam Jensen
Adam Saponara
Adam Thiede
Adrián Arroyo Calle
Aiden Grossman
Aleksei Nikiforov
Alex Denes
Alex Dowad
Alex McGrath
Alex Ryndin
Alex Xu (Hello71)
Alexander Brzoska
Alexander Mazuruk
Alexey Yerin
Andreas Kemnade
Andrei Jiroh Halili
Andrei Zavada
André Klitzing
Andy Hawkins
Andy Postnikov
Anjandev Momi
Antoine Martin
Anton Bambura
Antoni Aloy Torrens
Ariadne Conill
Arnavion
Aron Barath
Avis Orsetti
Bart Ribbers
Ben Westover
Bobby The Builder
Boris Faure
Bradford D. Boyle
Brian Vuyk
Caleb Connolly
Carlo Landmeter
Carmina16
ChemicalXandco
Chris Talbot
Christian Kohlschütter
Clayton Craft
Conrad Hoffmann
Consus
Daisuke Mizobuchi
Damian Kurek
Daniel Mizyrycki
Daniel Moch
Daniel Tobon
Daniele Parisi
Danny McClanahan
Dave Henderson
David Demelier
David Florness
David Heidelberg
Dekedro
Dermot Bradley
Devin Lin
Devin Stewart
Dhruvin Gandhi
Dmitry Zakharchenko
Dominika Liberda
Dominique Martinet
Drew DeVault
Duncan Bellamy
Dylan Van Assche
Edd Salkield
Elly Fong-Jones
Eloi Torrents
Emanuele Sorce
Eric Nemchik
Eric Roshan-Eisner
Fabian Affolter
Fiona Klute
FollieHiyuki
Francesco Camuffo
Francesco Colista
Frank Tributh
Gabriel Sanches
Galen Abell
Gavin Henry
Glenn Strauss
GreyXor
Grigory Kirillov
Guillaume Quintard
Guy Godfroy
Haelwenn (lanodan) Monnier
Hani Shawa
Henrik Grimler
Henrik Riomar
Holger Jaekel
Hugo Rodrigues
Hugo Wang
Ian Bashford
Ingo
Iskren Chernev
Iztok Fister Jr
J0WI
Jacob Panek
Jake Buchholz Göktürk
Jakob Hauser
Jakob Meier
Jakub Jirutka
Jakub Panek
Jan Jasper de Kroon
Jason Hall
Jeff Dickey
Jeremy Grosser
John Vogel
Jonas
Jonas Marklén
Jordan Christiansen
Jordan ERNST
JuniorJPDJ
Justin Berthault
Justin Klaassen
Kaarle Ritvanen
Kate
Kay Thomas
Kevin Daudt
Konstantin Kulikov
Krystian Chachuła
Lars Kellogg-Stedman
Laszlo Gombos
Lauren N. Liberda
Laurent Bercot
Lauri Tirkkonen
Leo
Leon Marz
Leonardo Arena
Luca Weiss
Lucas Ramage
Lucidiot
Maarten van Gompel
Magnus Sandin
Malte Voos
Marc Hassan
Marco Schröder
Marian Buschsieweke
Marino Pascual
Mark Pashmfouroush
Markus Kolb
Marten Ringwelski
Martijn Braam
Martin Thielecke
Martin Uddén
Marvin Preuss
Matthew T Hoare
Maxim Karasev
Michael Ekstrand
Michael Lyngbol
Michael Pirogov
Michael Truog
Michael Zimmermann
Michal Jirku
Michal Tvrznik
Michał Adamski
Michał Polański
Milan P. Stanić
Miles Alan
Minecrell
Mohammad Abdolirad
Natanael Copa
Nathan Rennie-Waldock
Newbyte
Ngô Ngọc Đức Huy
NickBug
Nico Schottelius
Nicolas Lorin
Noel Kuntze
Nulo
Oleg Titov
Oliver Smith
Olliver Schinagl
Otto Modinos
Pablo Correa Gómez
Patrick Gansterer
Paul Bredbury
Paul Spooren
Paulo Luna
Peter Shkenev
Peter van Dijk
Petr Fedchenkov
Philipp Arras
Pranjal Kole
Przemysław Romanik
QShen3
R4SAS
Ralf Rachinger
Rasmus Thomsen
Raymond Hackley
Risgit
Rob Blanckaert
Robert Adam
Robert Günzler
Rosie K Languet
Rudolf Polzer
Saarko
Saarko Sandomir
Saijin-Naib
Sam Whited
Sascha Brawer
Sashanoraa
Scott Robinson
Sean McAvoy
Sebastian Meyer
Sergey S
Simon Frankenberger
Simon Rupf
Simon Ser
Simon Zeni
Sodface
Stacy Harper
Stanislav Kholmanskikh
Steffen Nurpmeso
Stephen Abbene
Steve McMaster
Steven Honson
Stone Tickle
Sylvain Prat
Síle Ekaterin Liszka
Sören Tempel
TBK
Taner Tas
Thiago Perrotta
Thomas Deutsch
Thomas Faughnan
Thomas Kienlen
Thomas Liske
Tim Stanley
Timo Brasz
Timo Teräs
Timotej Lazar
Timothy Legge
Tom Tsagk
Tom Wieczorek
Tomio
Uli Roth
Umar Getagazov
Valery Ushakov
Wen Heping
Wesley van Tilburg
Will Sinatra
William Desportes
Wolf
Wolfgang Walther
Yann Autissier
Yo'av Moshe
Zach DeCook
Ziyao
alealexpro100
alice
alikates
ant
build@apk-groulx
crapStone
donoban
firefly-cpp
gay
guddaff
itd
j.r
james palmer
jenneron
jminer
jpdw34
knuxify
kpcyrd
kt programs
macmpi
messense
mio
misthios
mochaaP
nadvagauser
nibon7
omni
prspkt
psykose
ptrcnull
rubicon
skovati
takumin
techknowlogick
tetsumaki
tiotags
urain39
vin01
wener
xrs
Óliver García
Óliver García Albertos
</pre>
