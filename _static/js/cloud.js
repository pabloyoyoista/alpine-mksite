var filters = {}
var filtees = {}

// browser back/forward navigation resets page
var perfEntries = performance.getEntriesByType("navigation")
if (perfEntries[0].type === "back_forward") {
    location.reload(true);
}

function update_filter_values() {
    for (const f in filters)
        filters[f].value = filters[f].filter.value
}

function filter(x, update=true) {
    if (update)
        update_filter_values()

    for (const e of filters[x].filtees) {
        let display = ''
        // check all filters that apply to this filtee
        for (const f of filtees[e].filters) {
            if (filters[f].value == '*' || filtees[e].values.has(filters[f].value))
                continue

            display = 'none'
            break
        }
        filtees[e].filtee.style.display = display
    }
}

function apply_all_filters() {
    update_filter_values()
    for (const f in filters)
        filter(f, false)
}

function get_filtee_values(f) {
    var values = new Set()
    f.classList.forEach(c => {
        if (c.startsWith('v_'))
            values.add(c.slice(2))
    })
    return values
}

document.body.onload = function() {
    for (const filter of document.getElementsByClassName('filter')) {
        filters[filter.id] = {
            'filter': filter,
            'filtees': new Set(),
        }
        for (const filtee of document.getElementsByClassName(filter.id)) {
            filters[filter.id].filtees.add(filtee.id)
            if (filtee.id in filtees)
                filtees[filtee.id].filters.add(filter.id)
            else
                filtees[filtee.id] = {
                    'filtee': filtee,
                    'filters': new Set([filter.id]),
                    'values': get_filtee_values(filtee),
                }
        }
    }
    apply_all_filters()
}